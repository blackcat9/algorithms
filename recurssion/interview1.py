# Created by Black Cat
# Greatest Common Divisor

def gcd(a, b):
    assert int(a) ==a and int(b) == b, 'the number must be integer only'
    if a < 0:
        a = -1*a
    if b < 0:
        b = -1*b
    if int(b) == 0:
        return a
    else:
        return gcd(b, a%b)

print(gcd(-48, 18))