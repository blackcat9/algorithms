# Bubble Sort  Alforithm
# Created by Black Cat 
import math
from operator import length_hint

from numpy import c_

def bubble_sort(custom_list):
    for i in range(len(custom_list)-1):
        for j in range(len(custom_list)-i-1):
            if custom_list[j] > custom_list[j+1]:
                custom_list[j], custom_list[j+1] = custom_list[j+1],custom_list[j]
    print(custom_list)

def selection_sort(custom_list):
    for i in range(len(custom_list)):
        min_index = i
        for j in range(i+1, len(custom_list)):
            if custom_list[min_index] > custom_list[j]:
                min_index = j
        custom_list[i], custom_list[min_index] = custom_list[min_index],custom_list[i]
    print(custom_list)

def insertion_sort(custom_list):
    for i in range(1, len(custom_list)):
        key = custom_list[i]
        j = i-1
        while j>=0 and key < custom_list[j]:
            custom_list[j+1] = custom_list[j]
            j -= 1
        custom_list[j+1] = key
    print(custom_list)

def bucket_sort(custom_list):
    number_of_buckets = round(math.sqrt(len(custom_list)))
    max_value = max(custom_list)
    arr = []


    for i in range(number_of_buckets):
        arr.append([])
    for j in custom_list:
        index_b = math.ceil(j*number_of_buckets/max_value)
        arr[index_b-1].append(j)

    for i in range(number_of_buckets):
        arr[i] = insertion_sort(arr[i])

    k = 0
    for i in range(number_of_buckets):
        for j in range(length_hint(arr[i])):
            custom_list[k] = arr[i][j]
            k += 1
    return custom_list


def merge(custom_list, l, m, r):
    n1 = m - l + 1
    n2 = r - m

    L = [0] * (n1)
    R = [0] * (n2)

    for i in range(0, n1):
        L[i] = custom_list[l+i]

    for j in range(0, n2):
        R[j] = custom_list[m+1+j]

    
    i = 0
    j = 0
    k = l
    while i < n1 and j < n2:
        if L[i]  <= R[j]:
            custom_list[k] = L[i]
            i += 1
        else:
            custom_list[k] = R[j]
            j+= 1
        k += 1
    while i < n1:
        custom_list[k] = L[i]
        i += 1
        k += 1

    while j < n2:
        custom_list[k] = R[j]
        j += 1
        k += 1

def merge_sort(custom_list, l,r):
    if l < r:
        m = (l+(r-1))//2
        merge_sort(custom_list, l, m)
        merge_sort(custom_list, m+1, r)
        merge(custom_list, l, m, r)
    return custom_list

def partition(custom_list, low, high):
    i = low - 1
    pivot = custom_list[high]
    for j in range(low,high):
        if custom_list[j] <= pivot:
            i += 1
            custom_list[i], custom_list[j] = custom_list[j], custom_list[i]
    custom_list[i + 1], custom_list[high] = custom_list[high], custom_list[i+1]
    return (i+1)

def quick_sort(custom_list, low, high):
    if low < high:
        pivot = partition(custom_list, low, high)
        quick_sort(custom_list, low, pivot-1)
        quick_sort(custom_list,pivot+1, high)
def heapify(custom_list, n, i):
    smallest = i
    l = 2*i + 1
    r = 2*i + 2
    if l < n and custom_list[l] < custom_list[smallest]:
        smallest = l

    if smallest != i:
        custom_list[i], custom_list[smallest] = custom_list[smallest], custom_list[i]
        heapify(custom_list, n, smallest)


def heap_sort(custom_list):
    n = len(custom_list)
    for i in range(int(n/2)-1, -1, -1):
        heapify(custom_list,n, i)

    for i in range(n-1,0,-1):
        custom_list[i], custom_list[0] = custom_list[0], custom_list[i]
        heapify(custom_list, i, 0)
    custom_list.reverse()


c_list = [2,1,7,6,5,3,4,9,8]
bubble_sort(c_list)
selection_sort(c_list)
insertion_sort(c_list)
print(bucket_sort(c_list))
print(merge_sort(c_list, 0, 8))
quick_sort(c_list, 0, 8)
print(c_list)
heap_sort(c_list)
print(c_list)