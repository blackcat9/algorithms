#    Created by Black Cat
# Copyrights reserved

class Node:
    def __init__(self, value=None):
        self.value = value
        self.next = next


class LinkedList:
    def __init__(self):
        self.head = None

    def __iter__(self):
        curr_node = self.head
        while curr_node:
            yield curr_node
            curr_node = curr_node.next

class Stack:
    def __init__(self):
        self.LinkedList = LinkedList()

    def __str__(self):
        values = [str(x.value) for x in self.LinkedList]
        return '\n'.join(values)

    # is_empty method for Stack
    def is_empty(self):
        if self.LinkedList.head == None:
            return True
        else:
            return False

    # Push Method
    def push(self, value):
        node = Node(value)
        node.next = self.LinkedList.head
        self.LinkedList.head = node

    # Pop method for stack
    def pop(self):
        if self.is_empty():
            return "There is no element in the Stack to pop"
        else:
            node_value = self.LinkedList.head.value
            self.LinkedList.head = self.LinkedList.head.next
            return node_value



custom_stack = Stack()
print(custom_stack.is_empty())
custom_stack.push(1)
custom_stack.push(2)
custom_stack.push(3)
print(custom_stack)
print("---------------")
print(custom_stack.pop())
print(custom_stack)
