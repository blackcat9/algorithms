# Created by Black Cat
# Copyrights


class Stack:
    def __init__(self, max_size):
        self.max_size = max_size
        self.list = []

    def __str__(self):
        values = self.list.reverse()
        values = [str(x) for x in self.list]
        return '\n'.join(values)


    # is_empty method for stack


    def is_empty(self):
        if self.list == []:
            return True
        else:
            return False

    

    # is_full method for stack
    def is_full(self):
        if len(self.list) == (self.max_size):
            return True
        else:
            return False


    # Push method for stack
    def push(self,value):
        if self.is_full():
            return "The stack is full"
        else:
            self.list.append(value)
            return "The element has been successfully inserted"

    # Pop method for stack
    def pop(self):
        if self.is_empty():
            return "There are no elements in the stack"
        else:
            return self.list.pop()

    # peek method for stack
    def peek(self):
        if self.is_empty():
            return "There are no elements in the stack"
        else:
            return self.list[len(self.list) -1]

    # delete method for stack
    def delete(self):
        self.list = None



custom_stack = Stack(4)
print(custom_stack.is_empty())
print(custom_stack.is_full())
custom_stack.push(1)
custom_stack.push(2)
custom_stack.push(3)
print(custom_stack)
print(custom_stack.peek())