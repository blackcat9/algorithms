# Created by BlackCat

class Graph:
    def __init__(self, gdict=None):
        if gdict is None:
            gdict = {}
        self.gdict = gdict

    def add_edge(self, vertex, edge):
        self.gdict[vertex].append(edge)
    # Breatdth First Search  T(c) => O(v+e) S(c) => O(v+e)
    def bfs(self, vertex):
        visited = [vertex]
        queue = [vertex]
        while queue:
            de_vertex = queue.pop(0)
            print(de_vertex)
            for adjacent_vertex in self.gdict[de_vertex]:
                if adjacent_vertex not in visited:
                    visited.append(adjacent_vertex)
                    queue.append(adjacent_vertex)
    # Depth First Search
    def dfs(self, vertex):
        visited = [vertex]
        stack = [vertex]
        while stack:
            pop_vertex = stack.pop()
            print(pop_vertex)
            for adjacent_vertex in self.gdict[pop_vertex]:
                if adjacent_vertex not in visited:
                    visited.append(adjacent_vertex)
                    stack.append(adjacent_vertex)




custom_dict = { "a": ["b","c"],
                "b": ["a", "d", "e"],
                "c": ["a", "e"],
                "d": ["b", "c", "f"],
                "e": ["d", "f"],
                "f": ["d", "e"]

}

graph = Graph(custom_dict)
graph.bfs("a")
#graph.dfs("a")