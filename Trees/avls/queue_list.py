# Created by Black Cat


class Node:
    def __init__(self, value=None):
        self.value = value
        self.next = None

    def __str__(self):
        return str(self.value)

class LinkedList:
    def __init__(self):
        self.head = None
        self.tail = None


    def __iter__(self):
        curr_node = self.head
        while curr_node:
            yield curr_node
            curr_node = curr_node.next


class Queue:
    def __init__(self):
        self.linked_list = LinkedList()

    def __str__(self):
        values = [str(x) for x in self.linked_list]
        return ' '.join(values)

    # Enque methods
    def enqueue(self, value):
        new_node = Node(value)
        if self.linked_list.head == None:
            self.linked_list.head = new_node
            self.linked_list.tail = new_node
        else:
            self.linked_list.tail.next = new_node
            self.linked_list.tail = new_node

    def is_empty(self):
        if self.linked_list.head == None:
            return True
        else:
            return False

    def de_queue(self):
        if self.is_empty():
            return "There is no element in the queue"
        else:
            temp_node = self.linked_list.head
            if self.linked_list.head == self.linked_list.tail:
                self.linked_list.head = None
                self.linked_list.tail
            else:
                self.linked_list.head = self.linked_list.head.next
            return temp_node

    def peek(self):
        if self.is_empty():
            return "There is no element in the queue"
        else:
            return self.linked_list.head

    
    def delelte(self):
        self.linked_list.head = None
        self.linked_list.tail = None


