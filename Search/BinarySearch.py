# Created by Black Cat

# Searching algorithm - Binary Search
# Works with sorted arrays 
import math

def binary_search(array, value):
    start = 0
    end = len(array)-1
    middle = math.floor((start+end)/2)
    while not(array[middle]==value) and start<=end:
        if value < array[middle]:
            end = middle - 1
        else:
            start = middle + 1
        middle = math.floor((start+end)/2)
        #print(start, middle, end)
    if array[middle] == value:
        return middle
    else:
        return -1
        

custom_array = [8, 9, 12, 15, 17, 19, 20, 21, 28]
print(binary_search(custom_array, 15))