# Created by Black Cat

class Vertex:
    def __init__(self, node):
        self.id = node
        # Mark all nodes visited
        self.visited = False
        # Mark all nodes unvisited
        self.visited = False
    def add_neighbor(self, neighbor, G):
        G.add_edge(self.id, neighbor)
    def get_connections(self, G):
        return G.adj_matrix[self.id]
    def get_vertex_id(self):
        return self.id
    def set_vertex(self, id):
        self.id = id
    def set_visited(self):
        self.visited = True
    def __str__(self):
        return str(self.id)

class Graph:
    def __init__(self, num_vertices, cost=0):
        self.adj_matrix = [[-1]*num_vertices for _ in range(num_vertices)]
        self.num_vertices = num_vertices
        self.vertices = []
        for i in range(0, num_vertices):
            new_vertex = Vertex(i)
            self.vertices.append(new_vertex)
    def set_vertex(self,vtx,id):
        if 0 <= vtx < self.num_vertices:
            self.vertices[vtx].set_vertex(id)
    def get_vertex(self, n):
        for vertxin in range(0, self.num_vertices):
            if n == self.vertices[vertxin].get_vertex_id():
                return vertxin
        else:
            return - 1

    def add_edge(self, frm, to, cost=0):
        if self.get_vertex(frm) != -1 and self.get_vertex(to) != -1:
            self.adj_matrix[self.get_vertex(frm)][self.get_vertex(to)] = cost
            # For direcred graph do not add this
            self.adj_matrix[self.get_vertex(to)][self.get_vertex(frm)] = cost

    def get_vertices(self):
        vertices = []
        for vertxin in range(0, self.num_vertices):
            vertices.append(self.vertices[vertxin].get_vertex_id())
        return vertices

    def print_matrix(self):
        for u in range(0, self.num_vertices):
            row = []
            for v in range(0, self.num_vertices):
                row.append(self.adj_matrix[u][v])
                print(row)
    def get_edge(self):
        edges= []
        for v in range(0, self.num_vertices):
            for u in range(0, self.num_vertices):
                if self.adj_matrix[u][v] != -1:
                    vid = self.vertices[v].get_vertex_id()
                    wid = self.vertices[u].get_vertex_id()
                    edges.append((vid, wid, self.adj_matrix[u][v]))
        return edges

if __name__ == '__main__':
    G = Graph(5)
    G.set_vertex(0, 'a')
    G.set_vertex(1, 'b')
    G.set_vertex(2, 'b')
    G.set_vertex(3, 'c')
    G.set_vertex(4, 'e')
    print('GraphData:')
    G.add_edge('a', 'e', 10)
    G.add_edge('a', 'c', 20)
    G.add_edge('b', 'e', 30)
    G.add_edge('e', 'd', 50)
    G.add_edge('f', 'e', 60)
    print(G.print_matrix())
    print(G.get_edge())


