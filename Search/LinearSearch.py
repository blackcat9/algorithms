# Created by Black Cat

# Searhing Algorithm - Linear Search

#Good For sorted arrays 



def linear_search(array, value):
    for i in range(len(array)):
        if array[i] == value:
            return i
    return -1


print(linear_search([20,40,30,50,90], 90))
  