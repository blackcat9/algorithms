# Created by Black Cat


class Graph:
    def __init__(self, g_dict=None):
        self.g_dict = g_dict
        if g_dict is None:
            g_dict={}
            
    
    def add_edge(self, vertex, edge):
        self.g_dict[vertex].append(edge)
custom_dict = {"a": ["b", "c"],
                "b": ["a","d", "e"],
                "c": ["a", "e"],
                "d": ["b", "e", "f"],
                "e": ["d", "f", "c"],
                "f": ["d", "e"]
                    }

graph = Graph(custom_dict)
graph.add_edge("e", "c")
print(graph.g_dict["e"])