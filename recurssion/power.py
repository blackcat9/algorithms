# Created by Black Cat
# Powers of a Number by re recurssion

def power(base, exp):
    assert int(exp) == exp
    if int(exp) == 0:
        return 1
    elif exp < 0:
        return 1/base * power(base, exp+1)
    return base * power(base, exp - 1)


print(power(4, -2))
