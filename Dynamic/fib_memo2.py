# Created by Black Cat

# Dynamic Programming Solution for the Fibonacci Sequence 

def fibo(n):
    fib_table = [0,1]
    for i in range(2,n+1):
        fib_table.append(fib_table[i-1] + fib_table[i-2])
    return fib_table[n]
print(fibo(8))