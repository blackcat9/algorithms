#    Created by Ngakana Monyelo the "Black Cat"
# Copyright (c) Resewrved


class Stack:
    def __init__(self):
        self.list = []

    def __str__(self):
        values = self.list.reverse()
        values = [str(x) for x in self.list]
        return '\n'.join(values)




    # push method
    def push(self, value):
        self.list.append(value)
        return "The element has been successfully inserted"

        # isEmpty = True if Empty, False otherwise
    def is_empty(self):
        if self.list == []:
            return "True"
        else:
            return "False"

    # pop
    def pop(self):
        if self.is_empty():
            return "There is no element to Stack"
        else:
            return self.list.pop()


    # peek
    def peek(self):
        if self.list == []:
            return "There is no element to Stack"
        else:
            return self.list[len(self.list)-1]


    # delete
    def delete(self):
        self.list = None


custom_stack = Stack()
custom_stack.push(10)
custom_stack.push(20)
custom_stack.push(30)
custom_stack.push(40)

#print(custom_stack.pop())
#print(custom_stack)
print(custom_stack.peek())
print(custom_stack.delete())