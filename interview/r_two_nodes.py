# Created by Black Cat


class Graph:
    def __init__(self, gdict=None):
        if gdict is None:
            gdict = {}
        self.gdict = gdict

    def ad_adget(self, vertex, edge):
        self.gdict[vertex].append(edge)

    def check_route(self, start_node, end_node):
        visited = [start_node]
        queue = [start_node]
        path = False

        while queue:
            de_vertex = queue.pop(0)
            for adjacent_vertex in self.gdict[de_vertex]:
                if adjacent_vertex not in visited:
                    path = True
                    break
                else:
                    visited.append(adjacent_vertex)
                    queue.append(adjacent_vertex)
        return path


cutom_dict = {
    "a": ["c", "d", "b"],
    "b": ["j"],
    "c": ["g"],
    "d": [],
    "e": ["f", "a"],
    "f": ["i"],
    "g": ["d", "h"],
    "h": [],
    "i":  [],
    "j":  []
}

g = Graph(cutom_dict)
print(g.check_route("a", "e"))
