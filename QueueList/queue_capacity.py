#   Created by Black Cat
# Copyrights Resewrved


class Queue:
    def __init__(self, max_size):
        self.items = max_size * [None]
        self.max_size = max_size
        self.start = -1
        self.top = -1


    def __str__(self):
        values = [str(x) for x in self.items]
        return ' '.join(values)


    # is_full method
    def is_full(self):
        if self.top + 1 == self.start:
            return True
        elif self.start == 0 and self.top + 1 == self.max_size:
            return True
        else:
            return False

    # is empty method
    def is_empty(self):
        if self.top == -1:
            return True
        else:
            return False

    # Enqueue method
    def enqueue(self, value):
        if self.is_full():
            return "The queue is full"
        else:
            if self.top + 1 == self.max_size:
                self.top = 0
            else:
                self.top += 1
                if self.start == -1:
                    self.start = 0
            self.items[self.top] = value
            return "The element is inserted at the end of the queue"



    # Dequeue method
    def dequeue(self):
        if self.is_empty():
            return "There is no element in the queue"
        else:
            first_element = self.items[self.start]
            start = self.start
            if self.start == self.top:
                self.start = -1
                self.top = -1
            elif self.start + 1 == self.max_size:
                self.start = 0
            else:
                self.start += 1
                self.items[start] = None
                return first_element

    # Peek method
    def peek(self):
        if self.is_empty():
            return "There is no element in the queue"
        else:
            return self.items[self.start]

    








custom_queue = Queue(3)
custom_queue.enqueue(1)
custom_queue.enqueue(2)
custom_queue.enqueue(3)
print(custom_queue.is_full())
print(custom_queue.is_empty())
print(custom_queue)
print(custom_queue.dequeue())
print(custom_queue)
print(custom_queue.peek())
