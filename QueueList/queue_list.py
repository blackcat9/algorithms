# Created by Black Cat
# All copyrights reserved.



class Queue:
    def __init__(self):
        self.items = []


    def __str__(self):
        values = [str(x) for x in self.items]
        return ' '.join(values)


    #is_empty method
    def is_empty(self):
        if self.items == []:
            return True
        else:
            return False

    #Enqueue method for stack

    def enqueue(self, value):
        self.items.append(value)
        return "An element has been inserted at the end of the queue"

    # Dequeue method
    def dequeue(self):
        if self.is_empty():
            return "There is o element"
        else:
            return self.items.pop(0)

    # Peek method for stack
    def peek(self):
        if self.is_empty():
            return "There is no element in the QueueList"
        else:
            return self.items[0]

    # Delete method 
    def delete(self):
        self.items = None

    


custom_queue = Queue()
print(custom_queue.is_empty())
custom_queue.enqueue(1)
custom_queue.enqueue(2)
custom_queue.enqueue(3)
print(custom_queue)
print("----------------")
print(custom_queue.dequeue())
print(custom_queue)
print("----------------")
print(custom_queue.peek())






