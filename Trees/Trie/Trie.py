# Created by Black Cat
# Copyright (c) 2022 ArtScience

import re


class TrieNode:
    def __init__(self):
        self.children = {}
        self.end_of_string = False

class Trie(TrieNode):
    def __init__(self):
        self.root = TrieNode()

    def insert_string(self, word):
        current = self.root
        for i in word:
            ch = i
            node = current.children.get(ch)
            if node == None:
                node = TrieNode()
                current.children.update({ch:node})
            current = node
        current.end_of_string = True
        print("Successfully inserted")

    def search_string(self, word):
        current_node = self.root
        for i in word:
            ch = i
            node = current_node.children.get(ch)
            if node == None:
                return False
            current_node = node 
        if current_node.end_of_string == True:
            return True
        else:
            return False

def delete_string(root, word, index):
    ch = word[index]
    current_node = root.children.get(ch)
    can_node_be_deleted = False

    if len(current_node.children) > 1:
        delete_string(current_node, word, index+1)
        return False

    if index == len(word) -1:
        if len(current_node.children) >= 1:
            current_node.end_of_string = False
            return False
        else:
            root.children.pop(ch)
            return True

    if current_node.end_of_string == True:
        delete_string(current_node, word, index+1)
        return False

    can_node_be_deleted = delete_string(current_node, word, index+1)
    if can_node_be_deleted == True:
        root.children.pop(ch)
        return True
    else:
        return False





new_trie = Trie()
new_trie.insert_string("App")
new_trie.insert_string("Apl")
new_trie.insert_string("Appl")
delete_string(new_trie, "App", 0)

print(new_trie.search_string("App"))


