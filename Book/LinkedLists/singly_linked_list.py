# Node of a Singly Linked List
class Node:
    # constructor
    def __init__(self):
        self.data = None
        self.next = None
    # method for setting the data field of the node
    def set_data(self, data):
        self.data = data
    # method for getting the data field of the node
    def get_data(self):
        return self.data
    # method for setting next data field of the node
    def set_next(self, next):
        self.next = next
    # method for getting the next field of the node
    def get_next(self, next):
        return self.next
    #return true if the node point to another node
    def has_next(self):
        return self.next != None

# Sinly Linked list insertion_cdll
    def insert_at_begnning(self, data):
        new_node = Node()
        new_node.set_data(data)
        if self.length == 0:
            self.head = new_node
        else:
            new_node = set_next()