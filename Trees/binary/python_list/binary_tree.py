#Created by Black Created by


class BinaryTree:
    def __init__(self, size):
        self.custom_list = size * [None]
        self.last_use_index = 0
        self.max_size = size


    def insert_node(self, value):
        if self.last_use_index + 1 == self.max_size:
            return " the Binary Tree is full"
        self.custom_list[self.last_use_index+1] = value
        self.last_use_index += 1
        return "The value has been successfully inserted"

    def search_node(self, node_value):
        for i in range(len(self.custom_list)):
            if self.custom_list[i] == node_value:
                return "Success"
        return "Not Found"

    def pre_order_traversal(self,index):
        if index > self.last_use_index:
            return
        print(self.custom_list[index])
        self.pre_order_traversal(index*2)
        self.pre_order_traversal(index*2 + 1)
    
    def in_order_traversal(self,index):
        if index > self.last_use_index:
            return
        self.in_order_traversal(index*2)
        print(self.custom_list[index])
        self.in_order_traversal(index*2+1)

    def post_order_traversal(self,index):
        if index > self.last_use_index:
            return
        self.post_order_traversal(index*2)
        self.post_order_traversal(index*2+ 1)
        print(self.custom_list[index])

    def level_order_traversal(self,index):
        for i in range(index, self.last_use_index+1):
            print(self.custom_list[i])

    def delete_node(self, value):
        if self.last_use_index == 0:
            return "There is no node to delete"
        for i in range(1, self.last_use_index+1):
            if self.custom_list[i] == value:
                self.custom_list[i] = self.custom_list[self.last_use_index]
                self.custom_list[self.last_use_index] = None
                self.last_use_index -= 1
                return "The node has been successfully deleted"

    def delete_bt(self):
        self.custom_list = None
        return "The BT has been successfully deleted"


    



new_bt = BinaryTree(8)
print(new_bt.insert_node("Drinks"))
print(new_bt.insert_node("Hot"))
print(new_bt.insert_node("Cold"))
new_bt.insert_node("Tea")
new_bt.insert_node("Coffee")
#new_bt.pre_order_traversal(1)
#new_bt.in_order_traversal(1)
#new_bt.post_order_traversal(1)
#new_bt.level_order_traversal(1)
new_bt.delete_node("Coffee")
new_bt.level_order_traversal(1)
print(new_bt.delete_bt())