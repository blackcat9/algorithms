# Created 

import queue_list as queue

class TreeNode:
    def __init__(self, data):
        self.data = data
        self.left_child = None
        self.right_child = None

new_bt = TreeNode("Drinks")
leftchild = TreeNode("Hot")
tea = TreeNode("tea")
coffee = TreeNode("coffee")
leftchild.left_child = tea
leftchild.right_child = coffee
rightchild = TreeNode("Cold")
new_bt.left_child = leftchild
new_bt.right_child = rightchild

def pre_order_traversal(root_node):
    if not root_node:
        return
    print(root_node.data)
    pre_order_traversal(root_node.left_child)
    pre_order_traversal(root_node.right_child)

#pre_order_traversal(new_bt)

def in_order_traversal(root_node):
    if not root_node:
        return
    in_order_traversal(root_node.left_child)
    print(root_node.data)
    in_order_traversal(root_node.right_child)
    

#in_order_traversal(new_bt)

def post_order_traversal(root_node):
    if not root_node:
        return
    post_order_traversal(root_node.left_child)
    post_order_traversal(root_node.right_child)
    print(root_node.data)

#post_order_traversal(new_bt)

def level_order_traversal(root_node):
    if not root_node:
        return
    else:
        custom_queue = queue.Queue()
        custom_queue.enqueue(root_node)
        while not(custom_queue.is_empty()):
            root = custom_queue.de_queue()
            print(root.value.data)
            if (root.value.left_child is not None):
                custom_queue.enqueue(root.value.left_child)
                if (root.value.right_child is not None):
                    custom_queue.enqueue(root.value.right_child)


def search_bt(root_node, node_value):                           # O(n) space complexity
    if not root_node:
        return "The Binary Tree does not exist"
    else:
        custom_queue = queue.Queue()
        custom_queue.enqueue(root_node)
        while not(custom_queue.is_empty()):
            root = custom_queue.de_queue()
            if root.value.data == node_value:
                return "Success"
            if (root.value.left_child is not None):
                custom_queue.enqueue(root.value.left_child)

            if (root.value.right_child is not None):
                custom_queue.enqueue(root.value.right_child)
        return "Not found"

#print(search_bt(new_bt,"tea"))



def insert_node_bt(root_node, new_node):
    if not root_node:
        root_node = new_node
    else:
        custom_queue = queue.Queue()
        custom_queue.enqueue(root_node)
        while not(custom_queue.is_empty()):
            root = custom_queue.de_queue()
            if root.value.left_child is not None:
                custom_queue.enqueue(root.value.left_child)
            else:
                root.value.left_child = new_node
                return "Successfully Insterted"

            if root.value.right_child is not None:
                custom_queue.enqueue(root.value.right_child)
            else:
                root.value.right_child = new_node
                return "Successfully Inserted"


#new_node = TreeNode("Cola")
#print(insert_node_bt(new_bt, new_node))
#level_order_traversal(new_bt)   

def get_deepest_node(root_node):
    if not root_node:
        return
    else:
        custom_queue = queue.Queue()
        custom_queue.enqueue(root_node)
        while not(custom_queue.is_empty()):
            root = custom_queue.de_queue()
            if root.value.left_child is not None:
                custom_queue.enqueue(root.value.left_child)

            if root.value.right_child is not None:
                    custom_queue.enqueue(root.value.right_child)
        deepest_node = root.value
        return deepest_node


     

def delete_deepest_node(root_node, d_node):
    if not root_node:
        return
    else:
        custom_queue = queue.Queue()
        custom_queue.enqueue(root_node)
        while not(custom_queue.is_empty()):
            root = custom_queue.de_queue()
            if root.value.right_child is d_node:
                root.value.right_child = None
                return
            else:
                custom_queue.enqueue(root.value.right_child)
            if root.value.left_child is d_node:
                root.value.left_child = None
                return
            else:
                custom_queue.enqueue(root.value.left_child)


##new_node = get_deepest_node(new_bt)
#delete_deepest_node(new_bt, new_node)
#level_order_traversal(new_bt)

def delete_node_bt(root_node, node):                            # O(n)
    if not root_node:
        return "The Binary Tree does not exist"
    else:
        custom_queue = queue.Queue()
        custom_queue.enqueue(root_node)
        while not(custom_queue.is_empty()):
            root = custom_queue.de_queue()
            if root.value.data == node:
                d_node = get_deepest_node(root_node)
                root.value.data = d_node.data
                delete_deepest_node(root_node, d_node)
                return "The node has been successfully deleted"
            if (root.value.left_child is not None):
                custom_queue.enqueue(root.value.left_child)

            if (root.value.right_child is not None):
                custom_queue.enqueue(root.value.right_child)
        return "Failed to delete"



#delete_node_bt(new_bt, "Hot")
#level_order_traversal(new_bt)


def delete_bt(root_node):
    root_node.data = None
    root_node.left_child = None
    root_node.right_child = None
    return "The BT has been successfully deleted"


delete_bt(new_bt)
level_order_traversal(new_bt)